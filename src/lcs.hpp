#ifndef LCS_LCS_HPP
#define LCS_LCS_HPP

#include <string>
#include <string_view>

namespace lcs
{
  auto string(std::string_view, std::string_view) -> std::string;
}

#endif
