#include "lcs.hpp"

#include <algorithm>
#include <string>
#include <string_view>
#include <vector>

auto lcs::string(const std::string_view x, const std::string_view y) -> std::string
{
  using size_type = std::string_view::size_type;

  auto row = size_type(1);
  auto col = size_type(1);

  const auto matrix = [&]() {
    auto matrix = std::vector<std::vector<size_type>>(x.size() + 1, std::vector<size_type>(y.size() + 1));
    for (; row <= x.size(); ++row)
    {
      for (col = 1; col <= y.size(); ++col)
      {
        matrix[row][col] = x[row - 1] == y[col - 1] ? matrix[row - 1][col - 1] + 1
                                                    : std::max(matrix[row - 1][col], matrix[row][col - 1]);
      }
    }

    return matrix;
  }();

  const auto length = matrix[--row][--col];

  auto result = std::string();
  result.reserve(length);
  while (result.size() < length)
  {
    if (x[row - 1] == y[col - 1])
    {
      result += x[row - 1];
      --row;
      --col;
    }
    else
    {
      --(matrix[row - 1][col] > matrix[row][col - 1] ? row : col);
    }
  }
  std::reverse(result.begin(), result.end());

  return result;
}
