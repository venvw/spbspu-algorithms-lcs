#include <exception>
#include <iostream>
#include <string>

#include "lcs.hpp"

auto main() -> int
{
  try
  {
    auto lcs = std::string();
    if (std::cin >> lcs)
    {
      auto word = std::string();
      while (std::cin >> word)
      {
        lcs = lcs::string(lcs, word);
        if (lcs.empty())
        {
          return 0;
        }
      }
    }

    if (!lcs.empty())
    {
      std::cout << lcs << std::endl;
    }
  }
  catch (const std::exception& e)
  {
    std::cerr << e.what() << std::endl;
    return 2;
  }

  return 0;
}
